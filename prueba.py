#!/usr/bin/env python

# -*- coding: utf-8 -*-



import os, uuid



from M2Crypto import X509





import tornado.httpserver

import tornado.ioloop

import tornado.options

import tornado.web



import requests

from requests.auth import HTTPBasicAuth



from tornado.options import define, options



define("port", default=8888, help="run on the given port", type=int)



__UPLOADS__ = "/tmp/"



# mensaje hola mundo

class IndexHandler(tornado.web.RequestHandler):

	def get(self):

		greeting = self.get_argument('greeting', 'Hello')

		self.write(greeting + ', friendly user!\n')



# para obtener la version del servicio web

class VersionHandler(tornado.web.RequestHandler):

	def get(self):		

		# verify=False para no verificar el certificado del servidor web

		# auth=() para la autenticacion basica de HTTP

		r = requests.get('https://murachi.cenditel.gob.ve/Murachi/0.1/archivos/version', verify=False, auth=('admin', 'admin'))

		#r = requests.get('https://192.168.12.125:8443/Murachi/0.1/archivos/version', verify=False, auth=('admin', 'admin'))

		self.write(r.text)

		#r.json()

		#self.write(r.raise_for_status())

		





# para subir un archivo 

class UploadHandler(tornado.web.RequestHandler):

	def get(self):

		#files = {'file': open('/tmp/reports.txt', 'rb')}

		# subir un pdf

		files = {'file': open('/home/cenditel/desarrollo/murachi/convenioSUSCERTE-CENDITEL.pdf', 'rb')}

		r = requests.post('https://murachi.cenditel.gob.ve/Murachi/0.1/archivos/cargar', verify=False, auth=('admin', 'admin'), files=files)

		#r = requests.post('https://192.168.12.125:8443/Murachi/0.1/archivos/cargar', verify=False, auth=('admin', 'admin'), files=files)

		#self.write(r.text)

		r.json()

		fileId = r.json()['fileId']

		self.write(fileId)

		self.write('\n')



		#verificar si el documento esta firmado

		#r2 = requests.get('https://192.168.12.125:8443/Murachi/0.1/archivos/'+fileId, verify=False, auth=('admin', 'admin'), files=files)

		r2 = requests.get('https://murachi.cenditel.gob.ve/Murachi/0.1/archivos/'+fileId, verify=False, auth=('admin', 'admin'), files=files)

		self.write(r2.text)

		

# para obtener estadisticas basicas

class StatisticsHandler(tornado.web.RequestHandler):

	def get(self):

		r = requests.get('https://murachi.cenditel.gob.ve/Murachi/0.1/archivos/estadisticas', verify=False, auth=('admin', 'admin'))

		self.write(r.text)

		



		

		



# manejador principal

class MainHandler(tornado.web.RequestHandler):

	def get(self):

		self.write("""

			<form name="input" action="/loadfile" method="post" enctype="multipart/form-data" >

			archivo: <input type="file" name="fileToUpload"><br>

			<input type="submit" value="Enviar"/>

			</form>

			""")

	def write_error(self, status_code, **kwargs):

		self.write("Gosh darnit, user! You caused a %d error." % status_code)







class LoadHandler(tornado.web.RequestHandler):

	def post(self):

		#print self.request.arguments

		#self.write(self.request.arguments)

		#self.write("hola")

		#archivo = self.get_argument("file",'')



		fileinfo = self.request.files['fileToUpload'][0]

		fname = fileinfo['filename']

		self.write(fname+'\n')

		extn = os.path.splitext(fname)[1]

		cname = str(uuid.uuid4()) + extn

		fh = open(__UPLOADS__ +cname, 'w')

		fh.write(fileinfo['body'])

		self.write('archivo subido: '+cname+'\n')

		#self.finish(cname + " is uploaded! Check %s folder" %__UPLOADS__)



		mypage = u"""Hola mundo"""

		self.write(mypage)





if __name__ == "__main__":

	tornado.options.parse_command_line()

	#app = tornado.web.Application(handlers=[(r"/", IndexHandler)])

	app = tornado.web.Application(handlers=[(r"/", MainHandler), 

						(r"/index", IndexHandler),

						(r"/version", VersionHandler),

						(r"/upload", UploadHandler),

						(r"/estadisticas", StatisticsHandler),

						(r"/loadfile", LoadHandler)

	#					(r"/loadCertificate", LoadCertificateHandler)

						])

	

	http_server = tornado.httpserver.HTTPServer(app, ssl_options={"certfile": "server.crt", "keyfile": "server.key"})

	http_server.listen(options.port)

	tornado.ioloop.IOLoop.instance().start()

