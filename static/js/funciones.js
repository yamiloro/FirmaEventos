/**
 * Funcion para refrescar el captcha
 * @param element Recibe el parametro
*/

function refresh_captcha(element) {
    $form = $(element).parents('form');
    var url = location.protocol + "//" + window.location.hostname + ":" + location.port + "/captcha/refresh/";
    console.log(url)
    $.getJSON(url, {}, function(json) {
        $form.find('input[name="captcha_0"]').val(json.key);
        $form.find('img.captcha').attr('src', json.image_url);
    });

    return false;
}

/**
 * Función para obtener los usuarios de un evento
*/
function get_event_user() {
    var modal = false;
    var pasaporte = $('#id_pasaporte').val();
    if (pasaporte!='') {
        var routes = $(location).attr('pathname').split('/');
        var pk = routes[routes.length-1];
        var url = URL_USUARIO_EVENTO+pk+"/"+pasaporte;
        $.getJSON(url, function(data){
            if (Object.keys(data).length > 0) {
                construir_datos(data);   
            }
            else{
                simple_modal('Lo sentimos, no esta registrado para firmar');
            }
        }).fail(function(jqxhr, textStatus, error) {
            simple_modal('Petición fállida' + textStatus + ", " + error);
        })
    }
    else{
        simple_modal('Debe ingresar un pasaporte');
    }    
}

/**
 * Función para crear un modal sencillo
*/
function simple_modal(mensaje) {
    MaterialDialog.alert(
        mensaje,
        {
            title:'Alerta',
            buttons:{
                close:{
                    text:'cerrar',
                    className:'blue',
                }
            }
        }
    );
}

/**
 * Función para construir la data del participante
 * @param data Recibe los datos para crear la lista
*/
function construir_datos(data) {
    firma = data.firmo;
    data = data.datos;
    $('#datos_paricipante').html('');
    html = '<ul class="collection">'
    html += '<li class="collection-item"><b>Nombre: </b>'+data.nombres+'</li>';
    html += '<li class="collection-item"><b>Apellido: </b>'+data.apellidos+'</li>';
    html += '<li class="collection-item"><b>Pasaporte: </b>'+data.pasaporte+'</li>';
    html += '<li class="collection-item"><b>Correo: </b>'+data.correo+'</li>';
    html += '</ul>';
    html += '<iframe width="700px" height="600px" src="https://localhost:8443/Murachi/0.1/archivos/listadopdf/'+data.documento+'">';
    html += '</iframe><br/>';
    if (firma==true) {
        html += '<h4 class="red-text center">Ya firmó este documento</h4>'
    }
    else{
        html += '<ul class="collection" id="firma_documento">';      
        html += '<li class="collection-item"><label>Firma con Certificado de Software</label></div></li>';
    /*    html += '<li class="collection-item">';  
        html += '<div class="file-field input-field"><div class="btn waves-effect colorazul"><span>Selecionar Certificado</span><input id="p12" type="file">';
        html += '</div><div class="file-path-wrapper"><input class="file-path validate" type="text"></div></div>';*/
      /*  html += '<a type="button" id="subir_certificado" class="btn waves-effect blue darken-1" onclick="surbir_certificado()">';
        html += '<i class="material-icons left">mode_edit</i>Subir Certificado</a>';*/
        html += '</li>' 
      /*  html += '<li class="collection-item"><div id="respuesta"></li>';*/
        html += '<li class="collection-item"><div id="mensajeP12"></li>';
        html += '<li class="collection-item">';
        html += '<div class="input-field col s6"><input placeholder="password" id="clavePCKS12" type="password" class="validate"><label for="password">Password</label></div>'
        html += '<li class="collection-item">';
        html += '<a type="button" id="firmarcertificado" class="btn waves-effect colorazul" onclick="comprobarFirmaSoftware(\''+data.documento+'\')">';
        html += '<i class="material-icons left">mode_edit</i>Firmar con Certificado</a>';
        html += '</li>'
      /*  html += '</li>'
        html += '<li class="collection-item"><label>Firma por Tarjeta</label></div></li>';
        html += '<li class="collection-item">';
        html += '<a type="button" id="firmar" class="btn waves-effect blue darken-1" onclick="comprobarFirma(\''+data.documento+'\')">';
        html += '<i class="material-icons left">mode_edit</i> Firmar con Tarjeta</a>';   
        html += '</li>'*/
        html += '</ul>';
    }
    $('#datos_paricipante').html(html);
}

/**
 * Función para comprobar firma
 * @param fileId Recibe el id del documento
*/
function comprobarFirma(fileId){
    var routes = $(location).attr('pathname').split('/');
    var pk = routes[routes.length-1];
    $.ajax({
		type: 'GET',
        async: false,
		url:URL_COMPROBAR_FIRMA+pk,
		success: function(datos){
            if (datos.validate==true) {
                simple_modal(datos.mensaje);
            }
            else{
                $.ajax({
                    type: 'POST',
                    async: false,
                    url:URL_COMPROBAR_FIRMA+pk,
                    success: function(datos){                       
                        //datos = JSON.parse(datos)
                        if (datos) {
                            ObtenerCertificadoFirmanteMultiples(fileId,pk);
                        }
                        else{
  
                            simple_modal("Ocurrió un error al actualizar los datos");
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log('error: ' + textStatus);
                    }
                });
            }
        },
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error: ' + textStatus);
		}
	});
}

/**
 * Función para obtener el certificado del participante
 * @param fileId Recibe el id del documento
 * @param pk Recibe el id del evento
*/
function ObtenerCertificadoFirmanteMultiples(fileId,pk){
    var xPos = yPos= signaturePage = "";
    var lastSignature = false;
    
    $.ajax({
		type: 'GET',
        async: false,
		url:URL_ULTIMO_FIRMANTE+pk,
		success: function(datos){
            console.log(datos)
            if (datos.valid==true) {
                xPos = datos.data.posX;
                yPos = datos.data.posY;
                signaturePage = datos.data.page;
                lastSignature = true;
            }
            window.hwcrypto.getCertificate({lang: "en"}).then(
                function(response) {
                    var cert = response;
                    var parameters = "";
                    parameters = JSON.stringify({
                        "fileId":fileId,
                        "certificate":cert.hex,
                        "reason":"Certificado",
                        "location":"RedGealc",
                        "contact":"RedGealc",
                        "signatureVisible":"false",
                        "signaturePage": signaturePage,
                        "xPos": xPos,
                        "yPos": yPos,
                        "lastSignature":lastSignature
                        });				
        
                    // ahora llamar al ajax de obtener la resena del pdf
                    ObtenerHashPDFServerMultiples(parameters, cert, pk);	
        
                }, 
                function(err) {
                    var error;
                    if(err == "Error: user_cancel") {
                        error = "El usuario cancelo la operación"; 
                     }      
                     else if(err == "Error: no_certificates") {
                         error = "No hay certificado disponible";
                     }
                     else if(err == "Error: no_implementation") {
                         error = "No hay soporte para el manejo del certificado";
                    }else if(err == "Error: not_allowed"){
                        error = "ejecución no permitida por no usar https"
                    }
                    $.ajax({
                        type: 'POST',
                        async: false,
                        url:URL_COMPROBAR_FIRMA+pk,
                        success: function(datos){
                            console.log(datos);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log('error: ' + textStatus);
                        }
                    });                    
                    simple_modal(error);
                }
        
            );
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error: ' + textStatus);
		}
	});
}


/**
 * Función para obtener el hash y procesar la informacion
 * @param parameters Recibe los parametros
 * @param cert Recibe los certificados
 * @param pk Recibe el id del evento
*/
function ObtenerHashPDFServerMultiples(parameters,cert,pk){

	$.ajax({
		type: 'POST',
		contentType: 'application/json',				
		url:"https://localhost:8443/Murachi/0.1/archivos/pdfsqr",
		dataType: "json",
		data: parameters,		
		xhrFields: {withCredentials: true},
		headers: {"Authorization":"Basic YWRtaW46YWRtaW4="},
		success: function(data, textStatus, jqXHR){
			var json_x = data;
			var hash = json_x['hash']; 		
			var hashtype = "SHA-256";
			var lang = "eng";
			
			//Procesa la información
			window.hwcrypto.sign(cert, {type: hashtype, hex: hash}, {lang: lang}).then(
				function(signature) {
					FinalizarFirmaMultiples(signature.hex, pk);
      			}, 
      			function(err) {
					var error;
                    if(err == "Error: user_cancel") {
                        error = "El usuario cancelo la operación"; 
                     }      
                     else if(err == "Error: no_certificates") {
                         error = "No hay certificado disponible";
                     }
                     else if(err == "Error: no_implementation") {
                         error = "No hay soporte para el manejo del certificado";
                     }
                    $.ajax({
                        type: 'POST',
                        async: false,
                        url:URL_COMPROBAR_FIRMA+pk,
                        success: function(datos){
                            console.log(datos);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log('error: ' + textStatus);
                        }
                    });
                    simple_modal(error);
    	  	});
			
		},								
		error: function(jqXHR, textStatus, errorThrown){
			console.log('ajax error function: ' + jqXHR.responseText);
		}
		
	});	
}

/**
 * Función para enviar la firma al servidor
 * @param signature Recibe la firma
 * @param pk Recibe el id del evento
*/
function FinalizarFirmaMultiples(signature, pk){

	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url:"https://localhost:8443/Murachi/0.1/archivos/pdfs/resenas",
		dataType: 'json',
		data: JSON.stringify({"signature":signature}),
		xhrFields: {withCredentials: true},
		headers: {"Authorization":"Basic YWRtaW46YWRtaW4="},
		success: function(data, textStatus, jqXHR){
            actualizar_participante(data['signedFileId']);
            $.ajax({
                type: 'POST',
                async: false,
                url:URL_COMPROBAR_FIRMA+pk,
                success: function(datos){
                    console.log(datos);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log('error: ' + textStatus);
                }
            });
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error en pdfs/resenas: ' + textStatus);
            $.ajax({
                type: 'POST',
                async: false,
                url:URL_COMPROBAR_FIRMA+pk,
                success: function(datos){
                    console.log(datos);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log('error: ' + textStatus);
                }
            });
		}
	});

}

/**
 * Función para actualizar los datos del participante
 * @param id_documento Recibe el id del documento
*/
function actualizar_participante(id_documento) {
    var pasaporte = $('#id_pasaporte').val();
    var routes = $(location).attr('pathname').split('/');
    var pk = routes[routes.length-1];
    
    $.post(URL_ACTUALIZAR_PARTICIPACION,{'event_id':pk,'pasaporte':pasaporte,'serial':id_documento})
    .done(function(data){
        if (data.validate==true) {
            simple_modal(data.mensaje);
            $('#firmar').remove();
        }
        else{
            simple_modal(data.mensaje);
        }
    })
    .fail(function(err){
        console.log(err);
    });
}


/* Modificaciones para incorporar el murachi modificado (Murachi de centitel nuevo 
     y los metodos del murachi del institucion de ciencias para la firma por software) */
/* Firma por software */


/**
 * Función para comprobar firma
 * @param fileId Recibe el id del documento
*/
function comprobarFirmaSoftware(fileId){
    var routes = $(location).attr('pathname').split('/');
    var pk = routes[routes.length-1];
    $.ajax({
		type: 'GET',
        async: false,
		url:URL_COMPROBAR_FIRMA+pk,
		success: function(datos){
            if (datos.validate==true) {
                simple_modal(datos.mensaje);
            }
            else{
                $.ajax({
                    type: 'POST',
                    async: false,
                    url:URL_COMPROBAR_FIRMA+pk,
                    success: function(datos){                       
                        //datos = JSON.parse(datos)
                        if (datos) {
                            cargarCertificado(fileId,pk);
                        }
                        else{
  
                            simple_modal("Ocurrió un error al actualizar los datos");
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log('error: ' + textStatus);
                    }
                });
            }
        },
		error: function(jqXHR, textStatus, errorThrown){
			console.log('error: ' + textStatus);
		}
	});
}

function cargarCertificado(fileId,pk){

        var fileCertificadoId = "";
        $.ajax({            
            url: URL_BUSCAR_CERTIFICADO,
            type: "get",
            dataType: "json",
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            headers: {"Authorization":"Basic YWRtaW46YWRtaW4="},
            success: function(response) {

                $("#respuesta").html("");
                for(var key in response) {
                    $("#respuesta").html($("#respuesta").html()+"<br>"+response[key])
                    console.log(key);
                }
                console.log("exitoso")                
                console.log(response)
                fileCertificadoId = response["fileId"];

      
            },								
            error: function(jqXHR, textStatus, errorThrown){
                $("#respuesta").html("error function: " + jqXHR.responseText);
                console.log(jqXHR)   
                file = jqXHR.responseText       
            }
        })

        var formData = new FormData();   
        formData.append("clave", $("#clavePCKS12").val());
        $.ajax({            
            url: "https://localhost:8443/Murachi/0.1/archivos/p12clave",
            type: "post",
            dataType: "json",
            async: false,
            data: formData,
           /* xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progressUpload, false);
                    }
                    return myXhr;
            },*/
            cache: false,
            contentType: false,
            processData: false,
            headers: {"Authorization":"Basic YWRtaW46YWRtaW4="},
            success: function(response) {

                console.log('esta esla respuesta');
                console.log(response);
                $("#respuesta").html("");
                for(var key in response) {
                    $("#respuesta").html($("#respuesta").html()+"<br>"+response[key])
                    console.log(key);
                }

                console.log(response["error"]);
                //if(response["error"]==""){
                    almacenDigitalActivo = true;
                   // almacenId = response["fileId"];
                    $("#mensajeP12").html("Archivo almacen digital PCKS12 cargado<br>Se firmará a nombre de "+response["propietario"])
                    /*if(totalArchivos.length>0){
                        $("#password").attr("disabled",false);
                    }*/
                //}else{
                  // $("#mensajeP12").html("Se produjo un error al cargar el almacen digital PCKS12") 
                //}
                console.log(response)

                FinalizarFirmaMultiplesSoftware(fileId,fileCertificadoId,pk,$("#clavePCKS12").val())


            },								
                error: function(jqXHR, textStatus, errorThrown){
                    //console.log('error: ' + textStatus);
                    //var responseText = jQuery.parseJSON(jqXHR.responseText);
                    //console.log('ajax error function: ' + jqXHR.responseText);
                    $("#respuesta").html("error function: " + jqXHR.responseText);
                }
    })

}


function FinalizarFirmaMultiplesSoftware(fileId,almacenId,pk,clavePCKS12){
    var xPos = yPos= signaturePage = "";
    $.ajax({
		type: 'GET',
        async: false,
		url:URL_ULTIMO_FIRMANTE+pk,
		success: function(datos){
            console.log(datos)
            var xPos = yPos= signaturePage = "";
            var lastSignature = false;

            if (datos.valid==true) {
                xPos = datos.data.posX;
                yPos = datos.data.posY;
                signaturePage = datos.data.page;
                lastSignature = true;
            }
            console.log(datos)
            var parameters = JSON.stringify({
                "fileId":fileId,
                "certificate":"",
                "reason":"Certificado",
                "location":"RedGealc",
                "contact":"RedGealc",
                "pk12":true,
                "clavePCKS12":clavePCKS12,
                "almacenPCKS12":almacenId,
                "signatureVisible":false,
                "lly":0,
                "llx":0,
                "ury":0,
                "urx":0,
                "muestraImagen":false,
                "muestraImagenSola":false,
                "imagen":"",
                "signaturePage": signaturePage,
                "xPos": xPos,
                "yPos": yPos,
                "lastSignature":lastSignature,
                "tsa":false, 
                });
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json',
                    url:"https://localhost:8443/Murachi/0.1/archivos/pdfsPk",
                    dataType: 'json',
                    data: parameters,
                    xhrFields: {withCredentials: true},
                    headers: {"Authorization":"Basic YWRtaW46YWRtaW4="},
                    success: function(data, textStatus, jqXHR){
                        actualizar_participante(data['fileId']);
                        $.ajax({
                            type: 'POST',
                            async: false,
                            url:URL_COMPROBAR_FIRMA+pk,
                            success: function(datos){
                                console.log(datos);
                                $( "#firma_documento" ).remove();
                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                console.log('error: ' + textStatus);
                            }
                        });
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log('error en pdfs/resenas: ' + textStatus);
                        $.ajax({
                            type: 'POST',
                            async: false,
                            url:URL_COMPROBAR_FIRMA+pk,
                            success: function(datos){
                                console.log(datos);
                            },
                            error: function(jqXHR, textStatus, errorThrown){
                                console.log('error: ' + textStatus);
                            }
                        });
                    }
                });
        }
    })
}