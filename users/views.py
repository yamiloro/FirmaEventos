# -*- encoding: utf-8 -*-
"""!
Vista que controla los procesos de los usuarios

@author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
@copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
@date 09-01-2017
@version 1.0.0
"""
from django.core import serializers
from django import forms
from django.db.models import Q
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import (
    authenticate, logout, login
)

from django.contrib.admin.models import LogEntry, ADDITION, CHANGE, DELETION
from django.contrib.auth.models import (
    Group, Permission, User
)
from django.contrib.auth.views import redirect_to_login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.core.validators import validate_email
from django.core.urlresolvers import (
    reverse_lazy, reverse
)

from django.shortcuts import (
    render, redirect, get_object_or_404
)
from django.views.generic import TemplateView, UpdateView, View
from django.views.generic.base import RedirectView
from django.views.generic.edit import FormView
from multi_form_view import MultiModelFormView
from .forms import FormularioLogin
from .forms import UserForm
from .forms import CertificadoForm
from .forms import UpdateUserForm
from .forms import UpdateCertificadoForm
from base.views import LoginRequeridoPerAuth
from base.messages import MENSAJES_LOGIN, MENSAJES_START, MENSAJES_ARCHIVOS
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponseRedirect
import requests
import os

from users.models import (
    Certificado
)


def handle_uploaded_file(file, name):
    with open('%s/%s' % (settings.TMP, name), 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)

class LoginView(FormView):
    """!
    Muestra el formulario de ingreso a la aplicación 

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    form_class = FormularioLogin
    template_name = 'users_login.html'
    success_url = reverse_lazy('base:inicio')

    def form_valid(self, form):
        """
        Valida el formulario de logeo
        @return: Dirige a la pantalla inicial de la plataforma
        """
        usuario = form.cleaned_data['usuario']
        contrasena = form.cleaned_data['contrasena']
        try:
            validate_email(usuario)
            try:
                usuario = User.objects.get(email=usuario).username
            except:
                messages.error(self.request, 'No existe este correo: %s \
                                              asociado a una cuenta' % (usuario))
        except Exception as e:
            print (e)


        usuario = authenticate(username=usuario, password=contrasena)

        if usuario is not None:
            login(self.request, usuario)

            if self.request.POST.get('remember_me') is not None:
                # Session expira a los dos meses si no se deslogea
                self.request.session.set_expiry(1209600)
            #messages.info(self.request, MENSAJES_START['INICIO_SESION'] % (usuario.first_name, usuario.username))
        else:
            self.success_url = reverse_lazy('users:login')
            user = User.objects.filter(username=form.cleaned_data['usuario'])
            if user:
                user = user.get()
                if not user.is_active:
                    messages.error(self.request, MENSAJES_LOGIN['CUENTA_INACTIVA'])
                else:
                    messages.warning(self.request, MENSAJES_LOGIN['LOGIN_USUARIO_NO_VALIDO'])

        return super(LoginView, self).form_valid(form)


class LogOutView(RedirectView):
    """!
    Salir de la apliacion

    @author Ing. Leonel P. Hernandez M. (leonelphm at gmail.com)
    @copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
    @date 09-01-2017
    @version 1.0.0
    """
    permanent = False
    query_string = True

    def get_redirect_url(self):
        """!
        Dirige a la pantalla del login
        @return: A la url del login
        """
        logout(self.request)
        return reverse_lazy('users:login')

class UserFormView(FormView):
    
    form_class = UserForm
    template_name = "user_registration.html"
    second_form_class = CertificadoForm
    success_url = reverse_lazy('base:inicio')

    def get_context_data(self, **kwargs):
        context = super(UserFormView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form1' not in context:
            context['form1'] = self.second_form_class()
        return context


   # def get(self, request):
   #    form =  self.form_class(None)
   #    form1 = self.second_form_class(None)
   #    return render(request, self.template_name, {'form': form, 'form1': form1})

    
    def post(self, request, *args, **kwargs):

        form =  self.form_class(request.POST)
        form1 =  self.second_form_class(request.POST, request.FILES) 
        if form.is_valid() and form1.is_valid():
            consulta_api = None
            try:
                from botocore.vendored.requests.packages.urllib3.contrib import pyopenssl
                pyopenssl.extract_from_urllib3()
            except ImportError:
                pass        
            if len(request.FILES)>0:
                file =  request.FILES['certificado']
                handle_uploaded_file(request.FILES['certificado'], file)
                ruta = '%s/%s' % (settings.TMP, file)
                file = open(ruta, 'rb')
                files = {'upload': file}       
                print(file.name)   
                try:        
                    r = requests.post('https://localhost:8443/Murachi/0.1/archivos/p12cargar', verify=False, headers={'Authorization': 'Basic YWRtaW46YWRtaW4='}, files=files)
                    print(r.json())
                    consulta_api = r.json()['fileId']
                    # elimina el archivo si fue creado en la carpeta tmp
                    file.close()
                    os.unlink(ruta)
                except Exception as e:
                    print (e)
                    file.close()
                    os.unlink(ruta)
                    messages.error(self.request, MENSAJES_ARCHIVOS['ERROR_SUBIDA'] % ("el certificado"))
                    return self.render_to_response(self.get_context_data(form=form,form1=form1))
            user = form.save(commit=False)            
            username =  form.cleaned_data['username']
            password =  form.cleaned_data['password']
            email =  form.cleaned_data['email']
            first_name =  form.cleaned_data['first_name']
            last_name =  form.cleaned_data['last_name']
            user.first_name = first_name
            user.last_name = last_name
            user.username = username
            user.set_password(password)
            user.email = email
            user.save()
            certificado = form1.save(commit=False)                                    
            certificado.certificado = consulta_api
            pais = form1.cleaned_data['pais']
            certificado.pais = pais
            certificado.user = user
            certificado.save()
            return HttpResponseRedirect(self.get_success_url())
        else:            
            return self.render_to_response(self.get_context_data(form=form,form1=form1))



class UpdateUserFormView(FormView):
    """!
    Clase que permite actualizar los datos de un evento

    @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
    @copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
    @date 28-11-2017
    @version 1.0.0
    """
    model = Certificado
    form_class = UpdateUserForm
    template_name = 'user_update.html'
    success_url = reverse_lazy('users:perfil_consulta_participante')

    form_certificado = UpdateCertificadoForm

    def get_context_data(self, **kwargs):
        """!
        Metodo que permite cargar de nuevo valores en los datos de contexto de la vista

        @author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
        @copyright GNU/GPLv3
        @date 28-11-2017
        @param self <b>{object}</b> Objeto que instancia la clase
        @param kwargs <b>{object}</b> Objeto que contiene los datos de contexto
        @return Retorna los datos de contexto
        """
        context = super(UpdateUserFormView, self).get_context_data(**kwargs)
        user = User.objects.get(id=self.request.user.id)            
        certificado = Certificado.objects.get(user=user) 
        context['form'] =  self.form_class(instance=user)
        context['form1'] = self.form_certificado(instance=certificado)
        return context

    def post(self, request, *args, **kwargs):
        """!
        Metodo que permite actualizar y agregar mas participantes al evento

        @author Leonel P. Hernandez M (lhernandez at cenditel.gob.ve)
        @copyright GNU/GPLv3
        @date 29-11-2017
        @param request <b>{object}</b> Objeto que contiene la petición
        @return Retorna un mensaje de error o exito al success
        """

        userInstance = User.objects.get(id=self.request.user.id)           
        certificadoInstance = Certificado.objects.get(user=userInstance) 

        form =  self.form_class(request.POST, instance = userInstance)
        form1 =  self.form_certificado(request.POST, request.FILES, instance=certificadoInstance) 
        if form.is_valid() and form1.is_valid():
            consulta_api = None
            try:
                from botocore.vendored.requests.packages.urllib3.contrib import pyopenssl
                pyopenssl.extract_from_urllib3()
            except ImportError:
                pass        
            if  'certificado' in request.FILES and len(request.FILES['certificado'])>0:
                file =  request.FILES['certificado']
                handle_uploaded_file(request.FILES['certificado'], file)
                ruta = '%s/%s' % (settings.TMP, file)
                file = open(ruta, 'rb')
                files = {'upload': file}       
                print(file.name)   
                try:        
                    r = requests.post('https://localhost:8443/Murachi/0.1/archivos/p12cargar', verify=False, headers={'Authorization': 'Basic YWRtaW46YWRtaW4='}, files=files)
                    print(r.json())
                    consulta_api = r.json()['fileId']
                    # elimina el archivo si fue creado en la carpeta tmp
                    file.close()
                    os.unlink(ruta)
                except Exception as e:
                    print (e)
                    file.close()
                    os.unlink(ruta)
                    messages.error(self.request, MENSAJES_ARCHIVOS['ERROR_SUBIDA'] % ("el certificado"))
                    return self.render_to_response(self.get_context_data(form=form,form1=form1))
            user = form.save(commit=False)      
            user.id =  userInstance.id    
            username =  form.cleaned_data['username']
            #password =  form.cleaned_data['password']            
            email =  form.cleaned_data['email']
            first_name =  form.cleaned_data['first_name']
            last_name =  form.cleaned_data['last_name']
            user.first_name = first_name
            user.last_name = last_name
            user.username = username
            # print(password)
            # if "" not in password and password != None and type(password) is str:
            #     print("paso")
            #     user.set_password(password)            
            #user.set_password(password)
            user.email = email
            user.save()
            certificado = form1.save(commit=False)    
            certificado.id = certificadoInstance.id    
            pais = form1.cleaned_data['pais']     
            if  'certificado' in request.FILES and len(request.FILES['certificado'])>0:      
                certificado.certificado = consulta_api     
            certificado.pais = pais       
            certificado.user = user
            certificado.save()
            messages.error(self.request, messages.error(self.request, MENSAJES_LOGIN['USUARIO_ACTUALIZADO']))
            return HttpResponseRedirect(self.get_success_url())
        else:            
            return self.render_to_response(self.get_context_data(form=form,form1=form1))




class DetailUserForm(View):
    """!
    Muestra el detalle del evento

    @author Rodrigo Boet (rboet at cenditel.gob.ve)
    @copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
    @date 20-11-2017
    @version 1.0.0
    """
    model = Certificado
    template_name = "user.detail.html"


    def get(self, request):

        user = User.objects.get(id=self.request.user.id)          
        certificado = Certificado.objects.get(user=user) 

        context = {'certificado': certificado, "usuario":user}

        return render(request, 'user.detail.html', context)