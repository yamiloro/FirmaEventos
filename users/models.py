# -*- encoding: utf-8 -*-
"""!
Modelo que construye los modelos de datos de los usuarios

@author Ing. Leonel P. Hernandez M. (lhernandez at cenditel.gob.ve)
@copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
@date 19-11-2017
@version 1.0.0
"""
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver



class Pais(models.Model):
    """!
    Clase que contiene los paises

    @author Ing. Yamil Orozco. (yjorozco at cnti.gob.ve)
    @copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
    @date 25-10-2018
    @version 1.0.0
    """
    codigo = models.CharField(max_length=2)
    nombre = models.CharField(max_length=50)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        verbose_name = 'Pais'
        verbose_name_plural = 'Pais'
        db_table = 'users_pais'

    def __str__(self):
        """!
        Fucncion que muestra el pais
        @return Devuelve el pais
        """
        return self.nombre

class Certificado(models.Model):
    """!
    Clase que contiene el id de los cerificados

    @author Ing. Yamil Orozco. (yjorozco at cnti.gob.ve)
    @copyright <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>GNU Public License versión 3 (GPLv3)</a>
    @date 25-10-2018
    @version 1.0.0
    """
    certificado = models.CharField(max_length=150)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    pasaporte = models.CharField(max_length=50)  
    cargo = models.CharField(max_length=150)    
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        """!
            Clase que construye los meta datos del modelo
        """
        verbose_name = 'Certificado'
        verbose_name_plural = 'Certificado'
        db_table = 'users_certificado'

    def __str__(self):
        """!
        Fucncion que muestra el evento

        @return Devuelve el identificador del evento
        """
        return self.pasaporte


